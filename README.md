# Projet 4

## LIVRABLES

Dans un zip :
    * Le rapport d’analyse SEO du site, utilisant le modèle fourni, dans lequel on identifie clairement les 10 recommandations sélectionnées
    * Le code source complet du site amélioré
    * Un rapport d’optimisation comprenant une comparaison des résultats (y compris de la vitesse de chargement et l’accessibilité)

## ÉTAPES SEO

 1. L'audit préalable
 2. La recherche de mots-clés
    * Trouver les mots-clés de base
    * Décupler les idées
    * Analyser les résultats
 3. L'optimisation du site & la construction de votre machine à contenu
 4. La recherche de liens et de partenaires potentiels
 5. Le suivi de vos performances

## RECOMMANDATIONS SEO

    1. Contenu :
        * Mots par page : 400 pour page et 600 pour article
        * Meta title : 70 caractères max et description entre 132 et 160
        * Images pas trop lourdes
        * Alt présents

    2. Référencement :
        * Temps de chargement : 4s max
        * Responsive
        * Url de pages intelligentes
        * Robots : index, follow
        * Rich Snippets (si avis, produit, recette, musique, entreprise, etc...)

    3. Accesibilité :
        * règles WCAG
        * Repérages Aria

    4. Structure page :
        * Un titre   <h1>   et un seul avec mot clé exact
        * Plusieurs titres  <h2>faisant office de sous-parties avec mot-clé exact ou synonymes
        * Une ou plusieurs images avec mot-clé dans alt si cohérent
        * Plusieurs paragraphes, plutôt courts pour améliorer la lisibilité avec au moins une fois un mot-clé
        * Quelques liens internes  vers d'autres pages de votre site en lien avec le contenu de la page
        * Si possible, 1 ou 2 liens externes.

## MOTS CLÉS

 * Mots-clés de marque (nom de la marque)
 * Mots-clés généralistes (un seul mot, domaine)
 * Mots-clés d'information (question ou recherche de définition)
 * Mots-clés d'intention (verbes d'intention ou action)
 * Mots-clés locaux ou navgation (SEO local, lieux physiques)

## OUTILS NÉCESSAIRES
    ### Contenu dupliqué
        Siteliner
    
    ### Vitesse de chargement
        GTmetrics
        Pingdom

    ### Backlinks
        Moz Explorer
        Majestic
        SEMRush

    ### Mots-clés
        AnswerThePublic
        KWFinder

    ### Accesibilité
        TGPI
        Wave Evaluation Tool
        Axe (deque)